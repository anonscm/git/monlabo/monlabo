#!/bin/bash

#  For install
#-------------
#sudo apt install -y php-dev
#sudo pecl install ast

composer require  --dev phan/phan
composer require --dev php-stubs/wordpress-stubs
php vendor/bin/phan --dead-code-detection --unused-variable-detection --redundant-condition-detection --config-file scan/conf/.phan/config.php > scan/results/phan_scan_errors.txt
echo ERRORS: $(wc -l scan/results/phan_scan_errors.txt)
#--dead-code-detection --unused-variable-detection --redundant-condition-detection
#pecl install ast