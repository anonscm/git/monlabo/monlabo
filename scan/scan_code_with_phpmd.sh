#!/bin/bash
composer require  --dev  phpmd/phpmd
#php vendor/bin/phpmd . html cleancode,codesize,design,naming,unusedcode --exclude vendor,bin,coverage,documentation,tests,scan > phpmd_report.html

php vendor/bin/phpmd . html ./scan/conf/phpmd.WordPress.xml --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_WP_rules.html
WP_rules=$(cat scan/results/phpmd_WP_rules.html| grep "<section class='prb'" | wc -l)
echo "ERRORS WordPress_rules     :" $WP_rules
php vendor/bin/phpmd . html ./scan/conf/phpmd.WordPress2.xml --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_WP_rules2.html
WP_rules2=$(cat scan/results/phpmd_WP_rules2.html| grep "<section class='prb'" | wc -l)
echo "ERRORS WordPress_rules2    :" $WP_rules2
php vendor/bin/phpmd . html ./scan/conf/phpmd.WordPress_HSU.xml --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_WP_rules_HSU.html
WP_rules_HSU=$(cat scan/results/phpmd_WP_rules_HSU.html| grep "<section class='prb'" | wc -l)
echo "ERRORS WordPress_rules_HSU    :" $WP_rules_HSU
php vendor/bin/phpmd . html cleancode --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_cleancode.html
cleancode=$(cat scan/results/phpmd_cleancode.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. cleancode    :" $cleancode
php vendor/bin/phpmd . html codesize --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_codesize.html
codesize=$(cat scan/results/phpmd_codesize.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. codesize     :" $codesize
php vendor/bin/phpmd . html design --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_design.html
design=$(cat scan/results/phpmd_design.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. design       :" $design
php vendor/bin/phpmd . html naming --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_naming.html
naming=$(cat scan/results/phpmd_naming.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. naming       :" $naming
php vendor/bin/phpmd . html unusedcode --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_unusedcode.html
unusedcode=$(cat scan/results/phpmd_unusedcode.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. unusedcode   :" $unusedcode
php vendor/bin/phpmd . html controversial --exclude vendor,bin,coverage,documentation,tests,scan > scan/results/phpmd_controversial.html
controversial=$(cat scan/results/phpmd_controversial.html| grep "<section class='prb'" | wc -l)
echo "ERRORS Indep. controversial:" $controversial
total=$(( cleancode + codesize + design + naming + unusedcode + controversial ))
echo "TOTAL ERRORS Indep. = " $total

cat > scan/results/phpmd.html <<- EOM
<html><body>
<a href='phpmd_WP_rules.html'>WP_rules</a> $WP_rules<br/>
<a href='phpmd_WP_rules2.html'>WP_rules2</a> $WP_rules2<br/>
<a href='phpmd_WP_rules_HSU.html'>WP_rules_HSU</a> $WP_rules_HSU<br/>
<a href='phpmd_cleancode.html'>cleancode</a> $cleancode<br/>
<a href='phpmd_codesize.html'>codesize</a> $codesize<br/>
<a href='phpmd_design.html'>design</a> $design<br/>
<a href='phpmd_naming.html'>naming</a> $naming<br/>
<a href='phpmd_unusedcode.html'>unusedcode</a> $unusedcode<br/>
<a href='phpmd_controversial.html'>controversial</a> $controversial<br/>
Total = $total
</body></html>
EOM

echo "Open scan/results/phpmd.html to see errors."
