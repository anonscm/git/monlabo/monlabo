#!/bin/bash
composer require --dev phpstan/phpstan
composer require --dev szepeviktor/phpstan-wordpress
composer require --dev phpstan/extension-installer
vendor/bin/phpstan clear-result-cach -c scan/conf/phpstan.neon.dist
vendor/bin/phpstan analyse -c scan/conf/phpstan.neon.dist > scan/results/phpstan.txt
cat scan/results/phpstan.txt | grep "\[ERROR\]"
