#!/bin/bash
analyse(){
nbfiles=$(echo "$codefiles" | wc -l)
nbclass=$(echo "$codefiles" | grep "/class-" | wc -l)
nbtrait=$(echo "$codefiles" | grep "/trait-" | wc -l)
nblines=$(cat $codefiles | wc -l)
echo "Nombre de fichiers php : $nbfiles ($nbclass classes, $nbtrait traits)"

brutecode=$(cat $codefiles | sed -e 's/^[ \t]*//g' | grep -v "^//" | grep -v "^[ \t]*$" | awk '
/\/\*/ { inComment = 1 }
/\*\// { inComment = 0; next }
!inComment
'| grep -v "^[ \t]*$")
nblinecode=$(echo "$brutecode" | wc -l)
echo "Nombre de lignes : $nblines (dont $nblinecode lignes de code)"
}
echo "==== CODE ===="
codefiles="$(find .. | grep "\.php$" | grep -v "^\.\./vendor/" | grep -v "^\.\./tests/" | grep -v "^\.\./languages/"  | grep -v "^\.\./suplementary/" | grep -v "^\.\./scan/" | grep -v "/index.php$" )"
analyse
echo "=== TESTS ===="
codefiles="$(find ../../mon-laboratoire_tests/ | grep "mon-laboratoire_tests\/[^\/]*\.php$" | grep -v "\/index.php" )"
analyse
