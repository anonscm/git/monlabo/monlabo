#!/bin/bash
composer require --dev wp-coding-standards/wpcs
composer require --dev "squizlabs/php_codesniffer=*"
sudo phpcs --config-set installed_paths ~/lamp/www/sppin/wp-content/plugins/mon-laboratoire/vendor/wp-coding-standards/wpcs
php vendor/bin/phpcs --standard="Wordpress-Core" .