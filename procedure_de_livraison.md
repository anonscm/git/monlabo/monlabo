Procédure de livraison 
======================

1 - Faire le dernier commit
---------------------------
Bien penser à mettre à jour la version logicielle et l'historique :

* mon-laboratoire.php
* readme.txt (et la version derrière "Stable tag:" )

Vérifier qu'il ne manque pas de fichiers hors commit et faire le commit : 
*--> A FAIRE SUR mon-laboratoire/ et mon-laboratoire_tests/*

    git status # (véfifier la liste des "Fichiers non suivis")
    git add XXXX # eventuel
    git commit -a

Dernières vérifications après le commit :

    phpunit
    ./scan/scan_code_with_phan.sh
    ./scan/scan_code_with_phpstan.sh

2 - passer de la branche dev à la branche master
------------------------------------------------
Faire la fusion sur la branche master :
*--> A FAIRE SUR mon-laboratoire/ et mon-laboratoire_tests/*

    git checkout master
    git merge devX.X
    ... (ajustements éventuels)
    git commit -a

Poser le tag :
*--> A FAIRE SUR mon-laboratoire/ et mon-laboratoire_tests/*

    git tag -a "vX.X"
    git push origin --tags

Refaire les vérifications :

    phpunit
    ./scan/scan_code_with_phan.sh
    ./scan/scan_code_with_phpstan.sh

3 - Transférer du GIT SourceSup vers le SVN WordPress
-----------------------------------------------------
**ATTENTION, PHASE CRITIQUE**

Depuis le dossier de dev:

    cd ../../../../MonLaboOfficialSVN/
    meld trunk/ ../sppin/wp-content/plugins/mon-laboratoire/ &
    
Puis comparer les dossiers et transférer les différences. ########## AUTOMATISATION POSSIBLE

Vérifier qu'il ne manque pas de fichiers hors commit et préparer le commit :

    svn status
    svn add XXXXX
    svn delete XXXX
    svn status

4 - Test sommaire sur mes versions en prod
-------------------------------------------
* Ouvrir toutes les pages web pour garder en référence visuelle
* Faire un rsync -av --chown=www-data:www-data --delete --force --info=NONE,COPY,DEL,FLIST,MISC,NAME,SYMSAFE truck/ login@serveurprod:www/wordpress/wp-content/plugins/mon-laboratoire/
* Comparer l'effet sur les pages. Bien jouer avec le site.

5 - Préparer le mail de livraison
-----------------------------------------------------
Garder le mail en brouillon

6 - Lancer la version
---------------------
Puher sur le SVN WordPress
    svn commit

7 - Dernière vérification en prod
---------------------------------
Sur un site de prod:

* Effacer le dossier de l'extension WordPress mon-laboratoire (NE PAS LE DESINSTALLER!!! CELA EFFACERAIT LES DONNEES)
* Le recharger depuis l'interface wp-admin/plugins.php
* Vérfier le fonctionnement

8 - Lancer le mail de livraison
-------------------------------
* Envoyer le mail
* Confirmer son envoi à la liste de diffusion

9 - Mises à jour mineures
-------------------------
Créer le tag SVN :
	
    cd trunk
    svn update
	svn log . #(récupère le numéro de révision)
	REVISION=XXXXXX
	TAG="X.X"
	svn copy -r "$REVISION" "https://plugins.svn.wordpress.org/mon-laboratoire/trunk" "https://plugins.svn.wordpress.org/mon-laboratoire/tags/$TAG" -m "Creating tag $TAG from revision $REVISION"
	svn update

Mettre à jour monlabo.org


TODO : gérer stable tag