#!/bin/bash
pwd=$( pwd )
files="admin images includes languages changelog.txt index.php mon-laboratoire.css mon-laboratoire.php readme.txt uninstall.php"
mkdir /tmp/MonLabo
cp -arv $files /tmp/MonLabo
rm /tmp/MonLabo/languages/*~
tree /tmp/MonLabo
cd /tmp/MonLabo
zip -r mon_laboratoire_latest.zip *
cd $pwd
mv /tmp/MonLabo/mon_laboratoire_latest.zip ..
rm -rf /tmp/MonLabo

