apt install terser cleancss composer -y
composer update
terser Admin/js/MonLabo-admin.js -o Admin/js/MonLabo-admin.min.js --source-map
cleancss Admin/css/MonLabo-admin.css -o Admin/css/MonLabo-admin.min.css --source-map
cleancss Frontend/css/mon-laboratoire.css -o Frontend/css/mon-laboratoire.min.css --source-map