<?php
namespace MonLabo\Lib;

defined( 'ABSPATH' ) or die( 'No direct script access allowed' );

/**
 * trait \MonLabo\Lib\Singleton
 * 
 * Implements the Singleton pattern for any class using this trait.
 * This implementation is thread-safe and prevents serialization/deserialization.
 *
 * @package MonLabo\Lib
 */
trait Singleton {

	/**
     * Storage for Singleton instances
     * @var array<string, self>  // @phan-suppress-current-line PhanStaticPropIsStaticType
	 * @access private
	*/
	private static $_instances = array();

    /**
     * Lock for thread safety (avoid race conditions)
     *
     * @var array<string, bool>
     */
    private static $_locked = array();

	/**
     * Protected constructor to prevent direct instantiation
     * @throws \RuntimeException
     * @access protected
	 */
	protected function  __construct() {
		if ( isset( self::$_locked[static::class] ) ) {
            throw new \RuntimeException( sprintf( 'Singleton %s cannot be instantiated directly.', static::class ) );
        }
	}

	/**
	 * Prevent object cloning
     * @throws \RuntimeException
     * @return void
	 */
    final public function __clone() {
		throw new \RuntimeException( sprintf( 'Cloning is forbidden for singleton %s.', static::class ) );
	}

	/**
	 * Singletons should not be restorable from strings (Prevents deserialization).
     * @throws \RuntimeException
     * @return void
	 */
	final public function __wakeup() {
		throw new \RuntimeException( sprintf( 'Deserialization is forbidden for singleton %s.', static::class ) );
	}

    /**
     * Prevents serialization
     * @throws \RuntimeException
     * @return void
     */
    final public function __sleep() {
        throw new \RuntimeException( sprintf( 'Serialization is forbidden for singleton %s.', static::class ) );
    }

	/**
     * Returns the unique instance of the class
	 * As it sets to final it can't be overridden.
	 *
	 * @return self Singleton instance of the class.
	 */
	final public static function getInstance() {
		$class = static::class;
		if ( !isset( self::$_instances[ $class ] ) ) {
			self::$_locked[ $class ] = true;
			self::$_instances[ $class ] = new self(); // @phan-suppress-current-line PhanTypeInstantiateTraitStaticOrSelf
			unset( self::$_locked[ $class ] );
		}
		return self::$_instances[ $class ]; // @phan-suppress-current-line PhanTypeArraySuspiciousNullable
	}

    /**
     * Resets the singleton instance
     * Mainly useful for unit testing
     *
     * @return void
     */
	final public static function unsetInstance() {
		$class = static::class;
        unset( self::$_instances[ $class ] );
        unset( self::$_locked[ $class ] );
	}
}

?>
