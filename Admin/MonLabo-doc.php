<?php
defined( 'ABSPATH' ) or die( 'No direct script access allowed');

use MonLabo\Lib\{App, Lib, Options};

/**
 * Helper functions to generate common HTML elements
 */
class MonLabo_help_utils {	
	/**
     * Generate the code for an image
     * @param string $legend Legend to display
     * @param string $image_name Optional image name to display
     * @param int $image_width Optional image width
     * @return string Formatted HTML
     */
    public static function img( string $legend, string $image_name = '', int $image_width = 300): string {
        $image_html = '';
        if ($image_name) {
            $image_url = plugins_url( "images/{$image_name}", __FILE__ );
            $image_html = sprintf(
                '<p><a href="%s"><img width="%d" src="%s" alt="exemple" /></a></p>',
                $image_url, $image_width, $image_url
            );
        }    
        return '<div style="float:right;"><p>' . $legend . '&nbsp;:</p>' . $image_html . '</div>';
    }

	/**
     * Generate the code for an option list
     * @param string $title tilteof the list
     * @param array<string|int, string> $options list of options
     * @return string Formatted HTML
     */
    public static function options( string $title, array $options ): string {
		$list = array();
		foreach ($options as $name => $description ) {
			if ( is_int( $name ) ) { $name =''; }
			if ( !empty( $name ) ) { $name = '<em>' . $name . '</em>&nbsp;: ' ; };
			$list[] = $name . $description;
		}
		if ( !empty( $title ) ) { $title = '<p><strong>' . $title . "</strong></p>\n"; }
        return $title . "<ul>\n<li>" . implode( "</li>\n<li>", $list ) . "</li>\n</ul>\n" ;
    }
}

/**
 * Generate help pattern 1
 * @return string HTML code
 */
function MonLabo_items_multiples() {
	return MonLabo_help_utils::options( 'Options à items multiples', array( 'Les options <em>team, unit, categories</em> peuvent être des choix multiples en séparant les items par une virgule.') );
}

/**
 * Generate help pattern 2
 * @return string HTML code
 */
function MonLabo_options_facultatives_communes() {
	return MonLabo_help_utils::options( 'Options facultatives communes à [members_list], [members_table], [members_chart], [former_members_list], [former_members_table] et [former_members_chart]', array(
		'team="x"' 			=> 'N’affiche que les membres de l’équipe n°x (paramètre rempli automatiquement sur une page d’équipe)',
		'unit="x"' 			=> 'N’affiche que les membres de l’unité n°x (peut être une liste séparée par des virgules)',
		'categories="x"' 	=> 'N’affiche que les membres de la catégorie x (laisser vide pour choisir toutes les catégories, sinon choisir parmi <em>' . Lib::secured_implode( ",", App::get_MonLabo_persons_categories() ) . '</em>)'
	) );
}

/**
 * Generate help box for shortcode [members_list]
 * @return string HTML code
 */
function MonLabo_help_shortcode_members_list(): string {
	$to_display = '';
	$options = Options::getInstance();

	if ( !$options->uses['members_and_groups'] ) {
        return '<p>Désactivé</p>';
    }

	$to_display .= MonLabo_help_utils::img( '<strong>Rendu n°1</strong> (liste de tous les membres du laboratoire)', 'members_list.png' ) . '
	<p><strong>Où l’insérer&nbsp;?</strong> Sur une page d’équipe où sur la page de la liste des membres du laboratoire.</p>
	<p><strong>Contenu généré</strong> : Liste des membres du labo où d’une équipe.</p>
	' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
		'[members_list]'					=> 'Affiche la liste de tous les membres de l’équipe de la page en cours ou à défaut tous les membres du laboratoire. Ces membres sont séparés par catégories (Direction, Chercheurs permanents, Ingénieurs et techniciens, Posts doctorants et Étudiants).',
		'[members_list uniquelist="YES"]' 	=> 'Affiche la liste de tous les membres du laboratoire par ordre alphabétique sans séparation de catégories.',
		'[members_list team="3"]' 			=> 'Affiche la liste des membres de l’équipe n°3. Ces membres sont séparés en deux catégories : Chef(s) d’équipe(s) et Membres.',
	) ) . MonLabo_help_utils::options( 'Options facultatives spécifiques à [members_list]', array(
		'uniquelist="YES"' 				=> 'Si mis à YES, ne distingue plus les chefs/cheffes d’équipe, les directeurs/directrices ni les catégories (<em>NO</em> par défaut).',
		'display_direction="YES|NO"' 	=> 'Ajoute une catégorie <em>direction</em> séparant les directeurs/directrices (pour les unités) ou les chefs/cheffes (pour les équipes) des autres membres (<em>YES</em> par défaut).',
		'person="x"' 					=> 'force l’affichage des personnes d’ID x (peut être une liste séparée par des virgules).'
	) ) . MonLabo_help_utils::img( '<strong>Rendu n°2</strong> (liste de tous les membres d’une équipe)', 'members_list2.png' ) 
	. MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
	return $to_display;
}

/**
 * Generate help box for shortcode [members_table]
 * @return string HTML code
 */
function MonLabo_help_shortcode_members_table(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu n°1</strong> (liste de tous les membres du laboratoire)', 'members_table.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Sur une page d’équipe où sur la page de la liste des membres du laboratoire.</p>
		<p><strong>Contenu généré</strong> : Table des membres du labo où d’une équipe.</p>
		' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[members_table]'									=> 'Affiche la liste de tous les membres de l’équipe de la page en cours ou à défaut tous les membres du laboratoire. Ces membres sont séparés par catégories (Direction, Chercheurs permanents, Ingénieurs et techniciens, Posts doctorants et Étudiants).',
			'[members_table team="3" presentation="compact"]' 	=> 'Affiche la liste compacte des membres de l’équipe n°3. Ces membres sont séparés par catégories (Chercheurs permanents, Ingénieurs et techniciens, Posts doctorants et Étudiants).',
		) ) .  MonLabo_help_utils::options( 'Options facultatives spécifiques à [members_table]', array(
			'presentation="normal|compact'	=> 'Tableau complet ou résumé (<em>normal</em> par défaut).',
			'uniquelist="YES"'				=> 'Si mis à YES, ne distingue plus les chefs/cheffes d’équipe, les directeurs/directrices ni les catégories (<em>NO</em> par défaut).',
			'display_direction="YES|NO"'	=> 'Ajoute une catégorie <em>direction</em> séparant les directeurs/directrices (pour les unités) ou les chefs/cheffes (pour les équipes) des autres membres (<em>YES</em> par défaut).',
		) ) . MonLabo_help_utils::img( '<strong>Rendu n°2</strong> (liste compacte des membres d’une équipe)', 'members_table2.png' ) 
		. MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [members_chart]
 * @return string HTML code
 */
function MonLabo_help_shortcode_members_chart(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong> (liste de tous les membres du laboratoire)', 'members_chart.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Sur la page de la liste des membres du laboratoire.</p>
		<p><strong>Contenu généré</strong> : Organigramme des membres du labo.</p>
		' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[members_chart]'					=> 'Affiche l’organigramme de tous les membres du laboratoire. Ces membres sont séparés par catégories (Direction, Chercheurs statutaires, Ingénieurs et techniciens, Posts doctorants et Étudiants) en ligne et par équipe en colonne.',
		) ) . MonLabo_help_utils::options( 'Options facultatives spécifiques à [members_chart]', array(
			'[display_direction="YES|NO"]'		=> 'Ajoute une catégorie <em>direction</em> séparant les directeurs/directrices (pour les unités) ou les chefs/cheffes (pour les équipes) des autres membres (<em>YES</em> par défaut).',
		) ) . MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [former_members_list]
 * @return string HTML code
 */
function MonLabo_help_shortcode_former_members_list(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong> (liste de tous les anciens membres permanents du laboratoire)', 'alumni_list2.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong>  Sur une page d’équipe ou une page quelconque.</p>
		<p><strong>Contenu généré</strong> : Liste des anciens membres du labo.</p>
		' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[former_members_list]'																	=> 'Affiche la liste de tous les anciens membres du laboratoire.',
			'[former_members_list title="Former PhDs and Postdocs" categories="postdocs,students"]' => 'Affiche la liste de tous les anciens membres du laboratoire des catégories Doctorants et postdoctorants avec en titre Former PhDs and Postdocs.',
		) ) . MonLabo_help_utils::options( 'Options facultatives spécifiques à [former_members_list]', array(
			'title="y"' 	=> 'Affiche le titre y avant la liste.',
			'years="y"' 	=> 'Affiche les anciens membres avec les années de départ correspondantes (plage d’années séparées par un tiret - ou liste d’années séparées par une virgule).',
			'person="x"' 	=> 'force l’affichage des personnes d’ID x (peut être une liste séparée par des virgules).'
		) ) . MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [former_members_table]
 * @return string HTML code
 */
function MonLabo_help_shortcode_former_members_table(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong>', 'alumni_table.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Pas d’endroit spécifique nécessaire.</p>
		<p><strong>Contenu généré</strong> : Table des anciens membres du labo.</p>
		' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[former_members_table]'																	=> 'Affiche la table de tous les anciens membres du laboratoire.',
			'[former_members_table title="Former PhDs and Postdocs" categories="postdocs,students"]' 	=> 'Affiche la table de tous les anciens membres du laboratoire des catégories Doctorants et postdoctorants avec en titre Former PhDs and Postdocs.',
		) ) . MonLabo_help_utils::options( 'Options facultatives spécifiques à [former_members_table]', array(
			'title="y"' 	=> 'Affiche le titre y avant la liste.',
			'years="y"' 	=> 'Affiche les anciens membres avec les années de départ correspondantes (plage d’années séparées par un tiret - ou liste d’années séparées par une virgule).',
		) ) . MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [former_members_chart]
 * @return string HTML code
 */
function MonLabo_help_shortcode_former_members_chart(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong> (liste de tous les anciens membres du laboratoire)', 'alumni_chart.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Sur la page de la liste des membres du laboratoire.</p>
		<p><strong>Contenu généré</strong> : Organigramme des anciens membres du labo.</p>
		' .  MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[members_chart]'	=> 'Affiche l’organigramme de tous les anciens membres du laboratoire. Ces membres sont séparés par catégories (Direction, Chercheurs statutaires, Ingénieurs et techniciens, Posts doctorants et Étudiants) en ligne et par équipe en colonne.',
		) ) . MonLabo_help_utils::options( 'Options facultatives spécifiques à [former_members_chart]', array(
			'years="y"' 	=> 'Affiche les anciens membres avec les années de départ correspondantes (plage d’années séparées par un tiret - ou liste d’années séparées par une virgule).',
		) ) . MonLabo_options_facultatives_communes() . MonLabo_items_multiples() . '<div style="clear:both"></div>';
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [publications_list]
 * @return string HTML code
 */
function MonLabo_help_shortcode_publications_list(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( 'aucun' === $options->publication_server_type ) {
		$to_display .= '<p>Désactivé</p>';
		return $to_display;
	}
	$options4 = get_option( 'MonLabo_settings_group4' );
	$to_display .=  MonLabo_help_utils::img( '<strong>Rendu</strong>', 'publications_list2.png' ) .'
	<p><strong>Où l’insérer&nbsp;?</strong><br /> - Sur une page identifiée (équipe, unité ou page d’un personnel)<br /> - ou sur une page quelconque mais en étant contraint d’utiliser des "options complémentaire" pour identifier manuellement les publications.</p>
	<p><strong>Contenu généré</strong> : Insère automatiquement les publications de l’utilisateur ou de l’équipe issues d’une base de donnée extérieure qui peut être :<br />
	&nbsp;- soit <a href="http://hal.science/" target="_blank" rel="noopener noreferrer">HAL</a><br />
	&nbsp;- soit la <a href="' . $options4['MonLabo_DescartesPubmed_api_url'] . '" target="_blank" rel="noopener noreferrer">base de donnée du serveur Descartes Publi (Biomédicale)</a> (activable sur la <a href="admin.php?page=MonLabo_config&amp;tab=tab_configpublications">page d’option de publications</a>).</p>
	';
	$to_display .= MonLabo_help_utils::img( '', 'publications_list.png' );
	$to_display .= MonLabo_help_utils::options( 'Exemples d’utilisation', array(
		'[publications_list]' 									=> 'Génère une liste de publications.',
		'[publications_list title="Mes publications à moi"]' 	=> 'Ajoute ou change le titre initial.',
		'[publications_list years="2012-9999"]' 				=> 'Récupère les dernières publications jusqu’en 2012.',
	) ) .'<p><strong>Configuration préalable</strong>:</p>
	<p>Pour pouvoir interroger cette base de données extérieure, il faut au préalable indiquer dans MonLabo les identifiants des personnels, des équipes et éventuellement de la structure de la base de donnée extérieure :<br />
	Pour HAL (cf. <a href="https://aurehal.archives-ouvertes.fr/structure/index"> module de consultation de la liste des structures/équipes.</a>):<br />
	&nbsp;- champs "ID HAL de l’auteur" dans la <a href="admin.php?page=MonLabo_edit_members_and_groups&amp;tab=tab_person&amp;lang=all">liste des personnels</a> (rubrique "autres")<br />
	&nbsp;- champs "Identifiant HAL de l’équipe" dans la <a href="admin.php?page=MonLabo_edit_members_and_groups&amp;tab=tab_team&amp;lang=all">liste des équipes</a> (rubrique "autres")<br />
	&nbsp;- champs "Identifiant HAL de la structure" dans la <a href="admin.php?page=MonLabo_config&amp;tab=tab_person&amp;lang=all">Coordonnées</a><br />
	Pour la base de donnée Descartes Publi:<br />
	&nbsp;- champs "ID d’auteur Descartes Publi" dans la <a href="admin.php?page=MonLabo_edit_members_and_groups&amp;tab=tab_person&amp;lang=all">liste des personnels</a> (rubrique "autres")<br />
	&nbsp;- champs "ID d’equipe Descartes Publi" dans la <a href="admin.php?page=MonLabo_edit_members_and_groups&amp;tab=tab_team&amp;lang=all">liste des équipes</a> (rubrique "autres")
	</p>
	<p>Si Les données ne sont pas renseignées pour le serveur Descartes Publi, les publications sont issues de HAL.</p>
	' . MonLabo_help_utils::options( 'Options facultatives basiques', array(
		'title="Mon titre personnel"' 	=> 'Insère ou change le titre au début de la liste de publications.',
		'years="A"' 					=> 'Récupère les publications de l’année A (il est possible de pouvoir indiquer une plage d’années en séparant deux valeurs par le carractère "-").',
	) ) . MonLabo_help_utils::options( 'Options factultatives complémentaires', array(	
		'persons="X,Y,Z"' 				=> 'Force la récupération des publications pour les personnes n°X,Y et Z de l’extension MonLabo (utiliser le carractère "*" pour demander les publications de toutes les personnes entrées dans MonLabo). Si cette option est utilisée, les options <em>teams</em> et <em>units</em> sont ignorées.',
		'teams="X,Y,Z"' 				=> 'Force la récupération des publications pour les équipes n°X,Y et Z de l’extension MonLabo (utiliser le carractère "*" pour demander les publications de toutes les équipes entrées dans MonLabo).',
		'units="X,Y,Z"' 				=> 'Force la récupération des publications pour les unités n°X,Y et Z de l’extension MonLabo (utiliser le carractère "*" pour demander les publications de toutes les unités entrées dans MonLabo).',
		'lang="[fr|en]"' 				=> 'Affiche les publications dans la langue indiquée (par défaut c\'est la langue de l’interface de WordPress).',
		'limit="L"' 					=> 'Limite le nombre de publications affichées au nombre L.',
		'offset="F"' 					=> '<i>(ne marche pas avec HAL)</i> Décale l’affichage de F publications et affiche les publications restantes.',
		'base="[descartespubli|hal]"' 	=> 'force l’utilisation de la base <i>DescartesPubli</i> ou <i>hal</i> si les deux bases sont autorisées.',
		'debug="true"' 					=> 'Permet d’afficher, pour déboguage, l’adresse de la requête qui va récupérer les publications.',
	) ) . MonLabo_help_utils::options( 'Options facultatives expertes pour HAL <a href="https://hal.science/"><img width="61" height="30" class="wp-image-8 alignright wp-post-image" src="' . plugins_url( 'images/logoHAL.png', __FILE__ ) . '" alt="logo HAL" /></a>', array(
		'hal_struct="A;B;C"' 										=> 'écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications des structures au numéro HAL A, B et C (cf. <a href="https://aurehal.archives-ouvertes.fr/structure/index">module HAL de consultation de la liste des structures/équipes</a>).',
		'hal_idhal="A;B;C"' 										=> 'écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications des auteurs qui ont pour IdHal A, B, C (cf. <a href="https://aurehal.archives-ouvertes.fr/person/index">module HAL de consultation des auteurs</a>).',
		'hal_typepub="Type 1 de publication,Type 2 de publication"' => 'Récupère les publications du type indiqué (exemples de valeurs possibles:
		<em>ART, COMM, POSTER, OUV, COUV, DOUV, PATENT, REPORT, THESE...</em>). Par défaut tous les types sont selectionnés (<a href="https://api.archives-ouvertes.fr/ref/doctype">liste des types disponibles</a>).',
	) ) ;
	if ( 'hal' !== $options->publication_server_type ) {
		$to_display .=  MonLabo_help_utils::options( 'Options facultatives expertes pour Descartes Publi<img width="61" height="34" class="wp-image-8 alignright wp-post-image" src="' . plugins_url('images/DescartesPubli.logo.png', __FILE__) . '" alt="logo DescartesPubli" />', array(
			'descartes_alias="Unnom P"' 								=> '(écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications comprenant comme auteur l’alias indiqué (Souvent Nom et initiale du prénom).',
			'descartes_auteurid="Z"' 									=> '(écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications de l’auteur au numéro DescartesPubli n°Z (choix du n° <a href="' . $options4['MonLabo_DescartesPubmed_api_url'] . '?html_userslist">ici</a>).',
			'descartes_unite="X"' 										=> '(écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications de l’unité au numéro DescartesPubli n°X. (choix du n° <a href="' . $options4['MonLabo_DescartesPubmed_api_url'] . '?html_unitslist">ici</a>)',
			'descartes_equipe="Y"' 										=> '(écrase les paramètres <em>persons</em>, <em>teams</em> et <em>unit</em>) : Récupère les publications de l’équipe au numéro DescartesPubli n°Y (choix du n° <a href="' . $options4['MonLabo_DescartesPubmed_api_url'] . '?html_teamslist">ici</a>). Utiliser le carractère "*" pour demander toutes les publications des équipes entrées dans MonLabo.',
			'descartes_typepub="Type de publication"' 					=> 'Récupère les publications du type indiqué (choix parmis les valeurs possibles: <em>*, pub_journaux, pub_livres, pub_chapitres, pub_theses, pub_rapports, pub_presse, pub_communications, pub_brevets</em>). Par défaut le type choisi est pub_journaux.',
			'descartes_nohighlight' 									=> '(option sans paramètre) : Ne met pas en gras les alias des membre de l’unité ou de l’équipe sélectionnée. Par défaut, cette option sans paramètre est absente.',
			'descartes_orga_types="[aucun|par_titre|par_publication]"' 	=> 'Séléctionne la façon d’afficher les changement de types de publications.  Par défaut vaut <em>par_titre</em>.',
			'descartes_format="[html_default|html_hal]"' 				=> 'Séléctionne la façon d’afficher les publications. Par défaut vaut <em>html_default</em>.',
		) );
	}
	return $to_display;
}

/**
 * Generate help box for shortcode [teams_list]
 * @return string HTML code
 */
function MonLabo_help_shortcode_teams_list(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong>', 'teams_list.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Pas d’endroit spécifique nécessaire.</p>
		<p><strong>Contenu généré</strong> : Liste des équipes avec leur logos et leur thématiques.</p>
		' . MonLabo_help_utils::options( 'Exemples d’utilisation', array(
			'[teams_list]' 																		=> 'Affiche la liste de toutes les équipes.',
			'[teams_list group="3,4"]' 															=> 'Affiche la liste de toutes les équipes attachées au groupes d’équipes n°3 et 4.',
			'[teams_list group="3" teams_publications_page="Mes-publications-d-equipe.php"]' 	=> 'Affiche la liste de toutes les équipes attachées au groupe n°3 et ajoute un lien <em>"Team publication"</em> pointant vers la page Mes-publications-d-equipe.php',
		) ) . MonLabo_help_utils::options( 'Options facultatives', array(
			'group="x"' 					=> 'N’affiche que la liste des équipes  du groupe d’équipe au numéro indiqué (x peut être une liste séparée par des vigules)',
			'unit="x"' 						=> 'N’affiche que les équipes de l’unité n°x (x peut être une liste séparée par des vigules) (paramètre rempli automatiquement sur une page d’unité)',
			'team="x"' 						=> 'force l’affichage des équipes d’ID x (peut être une liste séparée par des virgules)',
			'teams_publications_page="url"' => 'Adresse de renvoi pour l’affichage optionnel des publications. Ajoute à chaque équipe un lien <em>"Team publication"</em> pointant vers l’url et ayant comme paramètre <em>"?equipe=N"</em> (N est le numéro de l’équipe dans la base Descartes Publi).',
		) );
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [perso_panel]
 * @return string HTML code
 */
function MonLabo_help_shortcode_perso_panel(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong>', 'perso_panel.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Au début de chaque page perso d’un utilisateur.</p>
		<p><strong>Contenu généré</strong> : Insère un encart avec les coordonnées de l’utilisateur. Les coordonnées sont récupérées depuis la liste des personnels dont le champs "ID page web" correspond à la page perso. L’image de l’utilisateur est l’"image à la une" de la page perso où est insérée cette balise.</p>
 		' . MonLabo_help_utils::options( 'Exemple d’utilisation', array(
			'[perso_panel]' 	=> 'Affiche encart.',
		) ) . MonLabo_help_utils::options( 'Options facultatives', array(
			'person="x"' => 'force l’affichage du perso_panel de l’utilisateur d’ID x.',
		) );
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

/**
 * Generate help box for shortcode [team_panel]
 * @return string HTML code
 */
function MonLabo_help_shortcode_team_panel(): string {
	$to_display = '';
	$options = Options::getInstance();
	if ( $options->uses['members_and_groups'] ) {
		$to_display .= MonLabo_help_utils::img( '<strong>Rendu</strong>', 'team_panel.png' ) . '
		<p><strong>Où l’insérer&nbsp;?</strong> Au début de chaque page d’équipe.</p>
		<p><strong>Contenu généré</strong> : Insère un encart avec le nom de l’équipe, les chefs d’équipe et les thématiques. Les informations sont récupérées depuis la liste des équipes dont le champs "ID page web" correspond à la page d’équipe.</p>
		' . MonLabo_help_utils::options( 'Exemple d’utilisation', array(
			'[team_panel]' 	=> 'Affiche encart.',
		) ) . MonLabo_help_utils::options( 'Options facultatives', array(
			'team="x"' => 'force l’affichage du team_panel de l’équipe d’ID x.',
		) );
		return $to_display;
	}
	$to_display .= '<p>Désactivé</p>';
	return $to_display;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
/**
 * Display help menu
 * @return void
 */
function MonLabo_help_render() {
	$inactive1_pre = '';
	$inactive1_post = '';
	$inactive1_final = '';

	$options = Options::getInstance();
	if ( !$options->uses['members_and_groups'] ) {
		$inactive1_pre .= '<s>';
		$inactive1_post .= '</s>';
		$inactive1_final .= ' ' . __( '(disabled)', "mon-laboratoire" );
	}
	$helplogo = '<span class="dashicons dashicons-editor-help" style="opacity: 0.5;">&nbsp;</span>';
	$members_list_info      	= boostrap_info_modal( $helplogo . '[members_list]',      	MonLabo_help_shortcode_members_list()      		);
	$members_table_info     	= boostrap_info_modal( $helplogo . '[members_table]',     	MonLabo_help_shortcode_members_table()     		);
	$members_chart_info     	= boostrap_info_modal( $helplogo . '[members_chart]',     	MonLabo_help_shortcode_members_chart()     		);
	$former_members_list_info	= boostrap_info_modal( $helplogo . '[former_members_list]', MonLabo_help_shortcode_former_members_list(), '[former_members_list] <small><small>ou [alumni_list]</small></small>');
	$former_members_table_info	= boostrap_info_modal( $helplogo . '[former_members_table]',MonLabo_help_shortcode_former_members_table(),'[former_members_table] <small><small>ou [alumni_table]</small></small>');
	$former_members_chart_info  = boostrap_info_modal( $helplogo . '[former_members_chart]',MonLabo_help_shortcode_former_members_chart(),'[former_members_chart] <small><small>ou [alumni_chart]</small></small>');
	$team_list_info         	= boostrap_info_modal( $helplogo . '[teams_list]',        	MonLabo_help_shortcode_teams_list()        		);
	$publications_list_info 	= boostrap_info_modal( $helplogo . '[publications_list]', 	MonLabo_help_shortcode_publications_list() 		);
	$perso_panel_info       	= boostrap_info_modal( $helplogo . '[perso_panel]',       	MonLabo_help_shortcode_perso_panel()       		);
	$team_panel_info        	= boostrap_info_modal( $helplogo . '[team_panel]',        	MonLabo_help_shortcode_team_panel()        		);

	echo( '<div class="wrap MonLabo_admin">' );
	echo( '<h1>'. __( 'Mini documentation', 'mon-laboratoire' ). ' :</h1>' );
	echo( '<h4>'. __( 'This WordPress plugin allows, on a unified interface, to manage the pages of teams and staff of a research laboratory.<br /> It simplifies the update of members and teams, their information and their publication list (extracted from HAL or an in-house database such as Paris Descartes database).', 'mon-laboratoire' ) . '</h4>' );
	$video_link = 'https://www.canal-u.tv/chaines/casuhal/afficher-une-liste-de-publications-dans-wordpress-les-plugins-hal-et-monlaboratoire?t=1137';
	echo('<div style="float:right;"><a href="' . $video_link . '"><img alt="Video thumbnail" src="' . plugins_url( 'images/casuhal-video.png', __FILE__ ) . '" width="250" height="135"></a></div>' );
	echo( '<h2>'. __( 'Presentation of functionalities in video', 'mon-laboratoire' ). ' :</h2>' );
	echo( '<p>' );
	printf( __( '<a href="%s">You can find here</a> a video presentation (in french) of functionalites at congress "Journées Casuhal 2022".', 'mon-laboratoire' ), $video_link );
	echo ( '</p>' );
	echo( '<h2>'. __( 'Shortcodes', 'mon-laboratoire' ). ' :</h2>' );
	echo( '<p>'. __( 'This plugin adds the below shortcodes. These "shortcodes" are to be inserted in the pages to automatically generate contents.', 'mon-laboratoire' ) . '</p>' );
	echo( '<ol>' );
	echo( '<li>' );
	printf(

		__( 'Display of staff...<ul><li>...%1$s that are active: by list %2$s, by table %3$s, by flowchart %4$s </li><li>... %5$s who are former members: by list %6$s, by table %7$s, by organization chart %8$s </li></ul>'	, 'mon-laboratoire'  )
		, $inactive1_pre
		, $inactive1_post . $members_list_info .  $inactive1_pre
		, $inactive1_post . $members_table_info . $inactive1_pre
	 	, $inactive1_post . $members_chart_info . $inactive1_final
	 	, $inactive1_pre
		, $inactive1_post . $former_members_list_info . $inactive1_pre
		, $inactive1_post . $former_members_table_info . $inactive1_pre
		, $inactive1_post . $former_members_chart_info . $inactive1_final
	);
	echo( '</li>' );
	printf(
		'<li> ' . __( '%s Displaying teams by list %s' , 'mon-laboratoire' ) . ' </li>'
		, $inactive1_pre
		, $inactive1_post . $team_list_info . $inactive1_final
	);

	$inactive2_pre = '';
	$inactive2_post = '';
	$inactive2_final = '';
	if ( 'aucun' === $options->publication_server_type ) {
		$inactive2_pre .= '<s>';
		$inactive2_post .= '</s>';
		$inactive2_final .= ' ' . __( '(disabled)', "mon-laboratoire" );
	}
	printf(
		'<li> ' . $inactive2_pre . __( 'Displaying publications %s', 'mon-laboratoire' ) . ' </li>'
		, $inactive2_post . $publications_list_info . $inactive2_final
	);

	printf(
		'<li> ' . __( '%s Displaying the header of personal pages %s or the header of team pages %s' , 'mon-laboratoire' ) . ' </li>'
		, $inactive1_pre
		, $inactive1_post . $perso_panel_info . $inactive1_pre
		, $inactive1_post . $team_panel_info . $inactive1_final
	);
	echo( '</ol>' );

	$MonLabo_menu_info = get_plugin_data( plugin_dir_path( __FILE__ ) .'../mon-laboratoire.php' );
	$user_data = get_userdata( get_current_user_id() );
	$user_email = '';
	if ( ! empty( $user_data ) ) {
		$user_email = $user_data->data->user_email; //@phan-suppress-current-line PhanRedefinedClassReference
	}
	echo ( '<h1>' . __( 'Help us by reporting that you are a user', 'mon-laboratoire' ) . ' :</h1>' );
	printf( __( 'You can allow the developers to publish on the <a href="%s">plugin site</a> that you are a user of MyLabo. This will do us a great service to show that this plugin is being used.', 'mon-laboratoire' ) . '<br />', $MonLabo_menu_info["PluginURI"] );
	echo( '<form method="get" action="http://monlabo.org/add_to_registred_users.php">' );
	echo( '<div class="input-group"><label>' . __( 'Email', 'mon-laboratoire' ) . ' ( ' . __( 'optional', 'mon-laboratoire' ) . ' )</label>' );
	echo( '<input type=\'text\' name=\'email\' value="' . $user_email . '" />' );
	echo( '<input type=\'hidden\' name=\'url\' value="' . site_url() . '" />' );
	echo( '<input type=\'hidden\' name=\'version\' value="' . $MonLabo_menu_info['Version'] . '" />' );
	echo( ' &nbsp;&nbsp;<input type="submit" name="submit" id="submit" class="button button-primary" value="' . __( 'Tell the authors that you are using the plugin on', 'mon-laboratoire' ) . ' ' . site_url() . '"  /></div></form>' );

	echo ( '<h1>' . __( 'Get informed', 'mon-laboratoire' ) . ' :</h1>' );
	_e( 'You can also subscribe to the following mailing lists', 'mon-laboratoire' );
	echo(  ' :<ul>' );
	echo( "<li><div class='input-group'><label>" . __( 'Announcements MonLabo', 'mon-laboratoire' ) . '</label>  (' . __( 'ex: new versions', 'mon-laboratoire' ) . ") " . boostrap_button_link( __( 'Subscribe now', 'mon-laboratoire' ), "https://listes.services.cnrs.fr/wws/subscribe/annonces_monlabo?previous_action=info", "button-primary" ) . "</div></li>" );

	echo( "<li><div class='input-group'><label>" . __( 'MonLabo mailing list', 'mon-laboratoire' ) . '</label> (' . __( 'discussions of the plugin users', 'mon-laboratoire' ) . ") " . boostrap_button_link( __( 'Subscribe now', 'mon-laboratoire' ), "https://listes.services.cnrs.fr/wws/subscribe/monlabo?previous_action=info", "button-primary" ) . "</div></li>" );
	echo( '</ul></div>' );
}
 ?>
