<?php
namespace MonLabo\Admin;
use MonLabo\Lib\{App};

defined( 'ABSPATH' ) or die('No direct script access allowed' );

/////////////////////////////////////////////////////////////////////////////////////
// GENERATE NOTICE MESSAGES ELEMENTS IN HTML
/////////////////////////////////////////////////////////////////////////////////////
/**
 * Class \MonLabo\Admin\Messages 
 * Class managing the display of notification messages in the WordPress admin interface.
 * @package MonLabo\Admin
 * @since 1.0.0
 */

 /* class Message {
	notice( string $type, string $title, string $message, string $mode = 'suround_with_p_tag' )
	echo_error_bad_migration()
	warning_if_necessary_unconfigured_parent( string $item_type, string $mode = 'hide' )
 }*/

class Messages {

    /**
     * Types de notifications supportés
     */
    private const NOTICE_TYPES = array(
        'error'   => array( 'class' => 'notice-error', 'dismissible' => false ),
        'warning' => array( 'class' => 'notice-warning', 'dismissible' => true ),
        'info'    => array( 'class' => 'notice-info', 'dismissible' => true ),
        'success' => array( 'class' => 'notice-success', 'dismissible' => true ),
	);

	/**
	* Generate 'error', 'warning' or 'info' alert in HTML
	* @param string $type type of notification ('error', 'warning', 'info', 'success')
	* @param string $title Title of box
	* @param string $message Content of box
	* @param string $mode if equal 'suround_with_p_tag' add <p> at the beginging and </p> at the end od $message
	* @return string HTML code
	*/
	public function notice( string $type, string $title, string $message, string $mode = 'suround_with_p_tag' ): string {
		$type = array_key_exists( $type, self::NOTICE_TYPES ) ? $type : 'info';
        $classes = array( 'notice', 'inline', self::NOTICE_TYPES[ $type ]['class'] );
        if ( self::NOTICE_TYPES[ $type ]['dismissible' ]) {
            $classes[] = 'is-dismissible';
        }
        $title_html = $title ? sprintf( '<h3>%s</h3>', esc_html( $title ) ) : '';
		$message_html = '';
		if ( $message ) {
        	$message_html = ( 'suround_with_p_tag' === $mode ) ? sprintf('<p>%s</p>', $message ) : $message;
		}
		return sprintf( '<div class="%s">%s%s</div>', esc_attr( implode( ' ', $classes ) ), $title_html, $message_html );
	}

	/**
	* Display a specific error message for bad migration
	* Configured by self::adin_init() to be called in action 'admin_notices'
	* @return void
	*/
	public function echo_error_bad_migration() {  // @phan-suppress-current-line PhanUnreferencedPublicMethod
		echo $this->notice( 'error',
				__( 'Plugin Mon-laboratoire:', 'mon-laboratoire' ),
				__( 'The plugin does not work because it was installed on an obsolete version of mon-laboratoire (&lt; v2.8). To solve the problem, before putting back this version, you must install version 2.8 of the plugin to ensure data migration.', 'mon-laboratoire' )
			);
	}

	/**
	* Display a specific error message for unconfigured parent page if necessary
	* @param string $item_type Item type ('person', 'team', 'thematic', 'unit', 'alumni')
	* @param string $mode Display mode ('hide' ou 'show')
	* @return string HTML code of the error message
	*/
	public function warning_if_necessary_unconfigured_parent( string $item_type, string $mode = 'hide' ): string {
		$retval = '';
		if ( $this->_should_display_warning( $item_type ) ){
			$classes = array( 'clear' );
            if ( $mode === 'hide' ) { $classes[] = 'MonLabo_hide'; }
            $retval .= sprintf(
                '<div id="MonLabo_noParentPageConfigured" class="%s">%s</div>',
                esc_attr( implode( ' ', $classes ) ),
                $this->notice(
                    'warning',
                    __('Parent pages not configured', 'mon-laboratoire'),
                    sprintf(
                        '<p>%s</p> %s',
                        __('You have not configured a parent page for new pages. By default, new pages will be saved at the root.', 'mon-laboratoire'),
                        $this->_get_configure_button_html()
                    ),
                    ''
                )
            );
		}
		return $retval;
	}

		/**
     * Generate HTML of configuration button
     * @return string HTML code of button
	 * @access private
     */
    private function _get_configure_button_html(): string {
        $url = add_query_arg( array( 'page' => 'MonLabo_config', 'tab' => 'tab_pages', 'lang' => 'all' ), admin_url( 'admin.php' ) );
        return sprintf( '<div class="button button-primary btn-sm"><a href="%s">%s</a></div>', $url , __('Configure', 'mon-laboratoire') );
    }

	/**
     * Get option key for a given type of element
     * @param string $item_type Type of element
     * @return string option key
	 * @access private
     */
    private function _get_option_item_key( string $item_type ): string {
        if ( $item_type === 'alumni' ) { $item_type = 'person'; }
        return $item_type === 'person' ? 'MonLabo_perso_page_parent' : "MonLabo_{$item_type}_page_parent";
    }

	/**
     * Verify if warning should be displayed
     * @param string $item_type Type of element
     * @return bool True if warning should be displayed
	 * @access private
     */
    private function _should_display_warning( string $item_type ): bool {
		$option_item = $this->_get_option_item_key( $item_type );
		$options10 = get_option( 'MonLabo_settings_group10' );
        return !isset( $options10[ $option_item ] ) || 
               ( $options10[ $option_item ] === App::NO_PAGE_OPTION_VALUE );
    }
}
?>
